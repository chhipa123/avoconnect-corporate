package com.avoconnectcorporate.Util;

import android.app.Dialog;
import android.content.Context;

import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;


import com.avoconnectcorporate.R;

import static com.avoconnectcorporate.Util.ProgressBarCustom.yesNoDialog;


/**
 * Created by vicky on 4/28/2016.
 */

public class DialogService {

    Context context;
    public void DialogService(Context context)
    {
        this.context=context;
    }
    public static void customYesDialog(View.OnClickListener clickListener,Context dialogContext,String message){
        yesNoDialog = new Dialog(dialogContext);
        yesNoDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        yesNoDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        //yesNoDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        yesNoDialog.setContentView(R.layout.logout_lay);
        yesNoDialog.setCanceledOnTouchOutside(true);
//        if(title!=null && title.length()>0){
//            if(title.equalsIgnoreCase("login")){
//                ((Button) yesNoDialog.findViewById(R.id.btnDialogYes)).setText("Login");
//                ((Button) yesNoDialog.findViewById(R.id.btnDialogNo)).setText("Cancel");
//            }
//            if(title.equalsIgnoreCase("cart")){
//
//            }
//        }


        //((Button) yesNoDialog.findViewById(R.id.btnDialogYes)).setText("Ok");
        //((Button) yesNoDialog.findViewById(R.id.btnDialogNo)).setText("Cancel");
        //((Button) yesNoDialog.findViewById(R.id.btnDialogNo)).setTextColor(ContextCompat.getColor(dialogContext, R.color.colorAppRed));

        ((Button) yesNoDialog.findViewById(R.id.btnDialogYes)).setOnClickListener(clickListener);
        ((Button) yesNoDialog.findViewById(R.id.btnDialogNo)).setOnClickListener(clickListener);
        ((TextView) yesNoDialog.findViewById(R.id.txtDialogMessage)).setText(message);
        //   ((TextView) yesNoDialog.findViewById(R.id.txtDialogTitle)).setText("");
        yesNoDialog.show();
    }

}
