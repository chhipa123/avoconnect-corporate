package com.avoconnectcorporate.BottomSheetFragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.avoconnectcorporate.R;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import static android.app.Activity.RESULT_OK;

public class CallMessageFragment extends BottomSheetDialogFragment implements View.OnClickListener{

    boolean isclick = false;
    EditText linktitle_lay, et_link;
    String name="",email="",mobile="",card_id="",share_url="",logo_url="",backgroundcolor="";
    ShareCardFragment.Callback callback;
    ShareCardFragment.CutCallback cutCallback;
    Dialog dialog_global;
    String phonenumber;
    Uri uri;
    private BottomSheetBehavior.BottomSheetCallback mBottomSheetBehaviorCallback = new BottomSheetBehavior.BottomSheetCallback() {

        @Override
        public void onStateChanged(@NonNull View bottomSheet, int newState) {
//            if (newState == BottomSheetBehavior.STATE_HIDDEN) {
//                dismiss();
//            }

        }

        @Override
        public void onSlide(@NonNull View bottomSheet, float slideOffset) {

        }
    };

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

    }

    @SuppressLint("RestrictedApi")
    @Override
    public void setupDialog(final Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        //Get the content View
        dialog_global=dialog;
        View contentView = View.inflate(getContext(), R.layout.item_callmessage, null);
        TextView txt_sharemsg=(TextView)contentView.findViewById(R.id.txt_sharemsg);
        TextView txt_sharecall=(TextView)contentView.findViewById(R.id.txt_sharecall);


        ImageView cross_img = (ImageView) contentView.findViewById(R.id.cross_img);

        dialog.setContentView(contentView);
        cross_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (cutCallback != null) {
                    cutCallback.cut_clickaction(0);
                }
                dialog.cancel();
//                Intent intent = new Intent();
//                getActivity().setResult(Activity.RESULT_CANCELED, intent);
//                getActivity().finish();
            }
        });



        Bundle bundle = this.getArguments();
        if (bundle != null) {
            phonenumber = bundle.getString("phonenumber", "");

            txt_sharemsg.setText("SMS "+phonenumber);
            txt_sharecall.setText("Call "+phonenumber);
        }


        txt_sharemsg.setOnClickListener(this);
        txt_sharecall.setOnClickListener(this);

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) contentView.getParent()).getLayoutParams();
        CoordinatorLayout.Behavior behavior = params.getBehavior();

        //Set callback
        if (behavior != null && behavior instanceof BottomSheetBehavior) {
            ((BottomSheetBehavior) behavior).setBottomSheetCallback(mBottomSheetBehaviorCallback);
        }
    }

    private void validation() {
        String getText = linktitle_lay.getText().toString();
        String getlink = et_link.getText().toString();
        if (getText.length() == 0) {
            Toast.makeText(getActivity(), getResources().getString(R.string.enter_link_title), Toast.LENGTH_SHORT).show();
        } else if (getlink.length() == 0) {
            Toast.makeText(getActivity(), getResources().getString(R.string.enter_link), Toast.LENGTH_SHORT).show();
        } else if (URLUtil.isValidUrl(getlink) == false) {
            Toast.makeText(getActivity(), getResources().getString(R.string.enter_valid_url), Toast.LENGTH_SHORT).show();
        } else {
            Intent intent = new Intent();
            intent.putExtra("value", getText);
            intent.putExtra("link", getlink);
            intent.putExtra("type", "important");
            getActivity().setResult(Activity.RESULT_OK, intent);
            getActivity().finish();
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.txt_sharemsg:
                if(phonenumber!=null)
                {
                    sendMMS(phonenumber);
                }

                break;
            case R.id.txt_sharecall:
                    if(phonenumber!=null)
                    {
                        Callnumber(phonenumber);
                    }
                break;
//            case R.id.txt_sharesms:
//
//                sendMMS();
////                Intent intent02=new Intent(getActivity(), SharingActitivtity.class);
////                intent02.putExtra("sharing_type","sms");
////                intent02.putExtra("name",name);
////                intent02.putExtra("email",email);
////                intent02.putExtra("mobile",mobile);
////                intent02.putExtra("card_id",card_id);
////                intent02.putExtra("share_url",share_url);
////                intent02.putExtra("logo_url",logo_url);
////                intent02.putExtra("backgroundcolor",backgroundcolor);
////                startActivityForResult(intent02,144);
//                break;

        }
    }

    private void Callnumber(String phonenumber) {
        try {
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse("tel:" + phonenumber));
            startActivity(intent);
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 144 && resultCode == RESULT_OK) {
            if (callback != null) {
                callback.clickaction(0);
            }
            dialog_global.dismiss();
            //getActivity().finish();
        }
    }

    public  void onCallBackReturn(ShareCardFragment.Callback callback)
    {
        this.callback=callback;
    }

    public interface Callback{
        void clickaction(int position);
    }



    public  void onCutCallBackReturn(ShareCardFragment.CutCallback callback)
    {
        this.cutCallback=callback;
    }

    public interface CutCallback{
        void cut_clickaction(int position);
    }



    public void sendMMS(String phonumber) {
        try {

            Intent smsIntent = new Intent(Intent.ACTION_VIEW);
            smsIntent.setType("vnd.android-dir/mms-sms");
            smsIntent.putExtra("address", phonumber);
            smsIntent.putExtra("sms_body",share_url);
            startActivity(smsIntent);

//        Intent intentsms = new Intent(Intent.ACTION_VIEW, Uri.parse("sms:"+"9806503950"));
//        intentsms.putExtra("sms_body",  share_url);
//        startActivity(intentsms);

//        Intent intent = new Intent(Intent.ACTION_SEND);
//        intent.putExtra("sms_body", share_url);
//        intent.putExtra("address", Uri.parse("sms:"+""));
//       intent.putExtra(Intent.EXTRA_STREAM, uri);
//        intent.setType("image/gif");
//         startActivity(Intent.createChooser(intent,"Send"));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
