package com.avoconnectcorporate.BottomSheetFragments;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.avoconnectcorporate.R;
import com.avoconnectcorporate.activity.AddEditCardActitivty;
import com.avoconnectcorporate.testingcard.TextActivity;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import static android.app.Activity.RESULT_OK;

public class AddCardsOcrManual extends BottomSheetDialogFragment implements View.OnClickListener {


    String myfirstcard="";
   Callback callback;
   CutCallback cutCallback;
    Dialog dialog_global;

    private BottomSheetBehavior.BottomSheetCallback mBottomSheetBehaviorCallback = new BottomSheetBehavior.BottomSheetCallback() {

        @Override
        public void onStateChanged(@NonNull View bottomSheet, int newState) {
//            if (newState == BottomSheetBehavior.STATE_HIDDEN) {
//                dismiss();
//            }

        }

        @Override
        public void onSlide(@NonNull View bottomSheet, float slideOffset) {

        }
    };

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


    }

    @SuppressLint("RestrictedApi")
    @Override
    public void setupDialog(final Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        //Get the content View
        dialog_global=dialog;
        View contentView = View.inflate(getContext(), R.layout.item_addcard, null);
        TextView txt_manually=(TextView)contentView.findViewById(R.id.txt_manually);
        TextView txt_scancard=(TextView)contentView.findViewById(R.id.txt_scancard);
        ImageView cross_img = (ImageView) contentView.findViewById(R.id.cross_img);

        dialog.setContentView(contentView);
        cross_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (cutCallback != null) {
                    cutCallback.cut_clickaction(0);
                }
                dialog.cancel();
            }
        });

        txt_manually.setOnClickListener(this);
        txt_scancard.setOnClickListener(this);

        Bundle bundle = this.getArguments();
        if(bundle!=null)
        {
            if(bundle.getString("myfirstcard")!=null)
            {
                myfirstcard=bundle.getString("myfirstcard");
                Log.e("===myfirstcard==",myfirstcard+"");
            }
        }
        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) contentView.getParent()).getLayoutParams();
        CoordinatorLayout.Behavior behavior = params.getBehavior();

        //Set callback
        if (behavior != null && behavior instanceof BottomSheetBehavior) {
            ((BottomSheetBehavior) behavior).setBottomSheetCallback(mBottomSheetBehaviorCallback);
        }
    }


    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.txt_manually:
                Intent intent = new Intent(getActivity(), AddEditCardActitivty.class);
                intent.putExtra("mycard","mycards");
                if(myfirstcard.length()>0)
                {
                    intent.putExtra("myfirstcard","myfirstcard");
                }
                startActivityForResult(intent, 144);
                break;
            case R.id.txt_scancard:
                Intent intent01 = new Intent(getActivity(), TextActivity.class);
                intent01.putExtra("mycard","mycards");
                if(myfirstcard.length()>0)
                {
                    intent01.putExtra("myfirstcard","myfirstcard");
                }
                startActivityForResult(intent01, 144);
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 144 && resultCode == RESULT_OK) {
            if (callback != null) {
                callback.clickaction(0);
            }
            dialog_global.dismiss();
            //getActivity().finish();
        }
    }

    public  void onCallBackReturn(Callback callback)
    {
        this.callback=callback;
    }

    public interface Callback{
        void clickaction(int position);
    }



    public  void onCutCallBackReturn(CutCallback callback)
    {
        this.cutCallback=callback;
    }

    public interface CutCallback{
        void cut_clickaction(int position);
    }
}
