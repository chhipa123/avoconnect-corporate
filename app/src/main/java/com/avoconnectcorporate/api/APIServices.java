package com.avoconnectcorporate.api;

import com.avoconnectcorporate.responsemodel.AddEvent_Model;
import com.avoconnectcorporate.responsemodel.AddLable_Modele;
import com.avoconnectcorporate.responsemodel.AddLocationTime_Modele;
import com.avoconnectcorporate.responsemodel.AddManualCard_model;
import com.avoconnectcorporate.responsemodel.AddNote_Modele;
import com.avoconnectcorporate.responsemodel.AddTag_Modele;
import com.avoconnectcorporate.responsemodel.AdditionalUpdate_Model;
import com.avoconnectcorporate.responsemodel.AditonalInfo_Modele;
import com.avoconnectcorporate.responsemodel.AskEmail_Model;
import com.avoconnectcorporate.responsemodel.AskNotification_Model;
import com.avoconnectcorporate.responsemodel.AutoShare_Model;
import com.avoconnectcorporate.responsemodel.BlockSearchModal;
import com.avoconnectcorporate.responsemodel.BusinessCardInfo;
import com.avoconnectcorporate.responsemodel.CardBackgroundColorChnage_Model;
import com.avoconnectcorporate.responsemodel.CardDetails_Model;
import com.avoconnectcorporate.responsemodel.CardModel;
import com.avoconnectcorporate.responsemodel.ChangePass_Model;
import com.avoconnectcorporate.responsemodel.ChangePassword_Model;
import com.avoconnectcorporate.responsemodel.CheckCardAvaiblability_Model;
import com.avoconnectcorporate.responsemodel.CheckRequested_Model;
import com.avoconnectcorporate.responsemodel.ContactAdd_Model;
import com.avoconnectcorporate.responsemodel.Createcard_Model;
import com.avoconnectcorporate.responsemodel.DeleteCard_Model;
import com.avoconnectcorporate.responsemodel.DeleteEvent_Model;
import com.avoconnectcorporate.responsemodel.DeleteExpereince_Model;
import com.avoconnectcorporate.responsemodel.DeleteRecivedCard_Modle;
import com.avoconnectcorporate.responsemodel.DeleteSkills_Model;
import com.avoconnectcorporate.responsemodel.Deleteaccount_Model;
import com.avoconnectcorporate.responsemodel.DesktopLogin_Model;
import com.avoconnectcorporate.responsemodel.EventSynup_Model;
import com.avoconnectcorporate.responsemodel.Expereince_Model;
import com.avoconnectcorporate.responsemodel.ForgetPass_Model;
import com.avoconnectcorporate.responsemodel.GEtPlanDetails;
import com.avoconnectcorporate.responsemodel.GenrateOrderID_Model;
import com.avoconnectcorporate.responsemodel.GetAllEvent_Model;
import com.avoconnectcorporate.responsemodel.GetCompany_Model;
import com.avoconnectcorporate.responsemodel.GetPaymentResponce;
import com.avoconnectcorporate.responsemodel.GetQualificaation_Model;
import com.avoconnectcorporate.responsemodel.GetUserProfile_Model;
import com.avoconnectcorporate.responsemodel.ListNotification_Model;
import com.avoconnectcorporate.responsemodel.LoginUser_Model;
import com.avoconnectcorporate.responsemodel.Logout_Model;
import com.avoconnectcorporate.responsemodel.NearByUser_Model;
import com.avoconnectcorporate.responsemodel.ProfileUpdate_Model;
import com.avoconnectcorporate.responsemodel.ReadNotification_Model;
import com.avoconnectcorporate.responsemodel.ReceivedCard_Model;
import com.avoconnectcorporate.responsemodel.RecivedCard_model;
import com.avoconnectcorporate.responsemodel.RegistrationOtp_Model;
import com.avoconnectcorporate.responsemodel.RequestOtp_Model;
import com.avoconnectcorporate.responsemodel.SearchUser_Model;
import com.avoconnectcorporate.responsemodel.SendNotificationRequest_Model;
import com.avoconnectcorporate.responsemodel.ShareCardTemplate_Model;
import com.avoconnectcorporate.responsemodel.ShareCard_Model;
import com.avoconnectcorporate.responsemodel.ShareMyUserId_Model;
import com.avoconnectcorporate.responsemodel.ShareNotificationUserid_Model;
import com.avoconnectcorporate.responsemodel.Signup_Model;
import com.avoconnectcorporate.responsemodel.Skills_Model;
import com.avoconnectcorporate.responsemodel.SocialLoginResponce;
import com.avoconnectcorporate.responsemodel.Submitotp_model;
import com.avoconnectcorporate.responsemodel.TemplateColorModel;
import com.avoconnectcorporate.responsemodel.TwoWayAuth_Model;
import com.avoconnectcorporate.responsemodel.UnreadNotficationCount_Model;
import com.avoconnectcorporate.responsemodel.UpdateAddtionalInfo_Model;
import com.avoconnectcorporate.responsemodel.UpdateCard_Model;
import com.avoconnectcorporate.responsemodel.UpdateEmail_Model;
import com.avoconnectcorporate.responsemodel.UpdateLatlng_Model;
import com.avoconnectcorporate.responsemodel.UpdateMobile_Modele;

import java.util.LinkedHashMap;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.Headers;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;
import retrofit.http.Query;
import retrofit.http.QueryMap;
import retrofit.mime.TypedInput;

public interface APIServices {

    @POST("/public/user/saveSignUpUser")
    void signup(@Body TypedInput body, Callback<Signup_Model> blogOutput);


    @PUT("/public/user/sendForgotpasswordEmail")
    void forgetpasswrod(@Query("email") String email, @Body TypedInput body, Callback<ForgetPass_Model> blogOutput);

    @PUT("/public/user/changePassword")
    void changepassword(@Body TypedInput body, Callback<ChangePass_Model> blogOutput);

    @Headers("Content-Type: application/json")
    @POST("/oauth/token")
    void loginuser(@Body TypedInput body, @QueryMap(encodeValues = false) LinkedHashMap<String, String> queryMap, Callback<LoginUser_Model> loginUser_modelCallback);


    @POST("/public/user/createCard")
    void createcard(@Body TypedInput body,Callback<Createcard_Model> createcard_modelCallback);

    @POST("/public/user/socialUserRegister")
    void socialLogin(@Body TypedInput body, Callback<SocialLoginResponce> socialLoginResponceCallback);

    @GET("/public/user/getCardList")
    void getCardList(@Header("Authorization") String token,@Query("userId") String userId,Callback<CardModel> cardModelCallback);


    @GET("/card/getCardDataByCardId")
    void getcarddetails(@Header("Authorization") String token,@Query("cardId") String card_id, Callback<CardDetails_Model> socailLogin_modelCallback);

    @PUT("/public/user")
    void  updatecard(@Header("Authorization") String token,@Query("cardId") String card_id,@Body TypedInput body,
                     Callback<UpdateCard_Model>updateCard_modelCallback);

    @DELETE("/card")
    void deletecard(@Header("Authorization") String token,@Query("cardId") String card_id,Callback<DeleteCard_Model>deleteCard_modelCallback);


    @GET("/card/shareCardByUserId")
    void sharecard_id(@Header("Authorization") String token, @Query("senderId") String senderId, @Query("receiverId") String receiverId,
                      @Query("cardId")String cardId, Callback<ShareCard_Model>shareCard_modelCallback);



    @POST("/card/receivedCardByUserId/{pageNumber}/{pageSize}")
    void receivedcards(@Header("Authorization") String token, @Body TypedInput body,
                       @Path("pageNumber") int pageNumber,@Path("pageSize")int pageSize,
                       Callback<ReceivedCard_Model>receivedCard_modelCallback);




    @DELETE("/oauth/revokeToken")
    void logoutapi(@Header("Authorization") String token,Callback<Logout_Model>logout_modelCallback);


    @GET("/user/{userId}")
    void getuserprofile(@Header("Authorization") String token,@Path("userId")String userid,Callback<GetUserProfile_Model>getUserProfile_modelCallback);

    @POST("/user/experience")
     void addexperience(@Header("Authorization") String token,@Body TypedInput body,Callback<Expereince_Model>expereince_modelCallback);

    @POST("/user/skill")
    void addskill(@Header("Authorization") String token,@Body TypedInput body,Callback<Skills_Model>expereince_modelCallback);

    @PUT("/user/profile")
    void updateprofile(@Header("Authorization") String token,@Body TypedInput body,Callback<ProfileUpdate_Model>profileUpdate_modelCallback);

    @DELETE("/user/skill/{userId}/{skillId}")
    void deleteskills(@Header("Authorization") String token,@Path("userId")String userid,@Path("skillId")String skillId,Callback<DeleteSkills_Model>deleteSkills_modelCallback);

    @DELETE("/user/experience/{userId}/{userExperienceId}")
    void deleteexpeirence(@Header("Authorization") String token,@Path("userId")String userid,@Path("userExperienceId")String skillId,Callback<DeleteExpereince_Model>deleteSkills_modelCallback);


    @POST("/card/additionalInfo/addNotes")
    void addnote(@Header("Authorization") String token, @Query("userId") String userid, @Query("cardId")String cardId,
                 @Body TypedInput body, Callback<AddNote_Modele>addNote_modeleCallback);

    @GET("/card/additionalInfo/addLabel")
    void addlable(@Header("Authorization") String token, @Query("userId") String userid, @Query("cardId")String cardId,
                  @Query("label")String label,
                  Callback<AddLable_Modele>addNote_modeleCallback);

    @GET("/card/additionalInfo/addLeads")
    void addtags(@Header("Authorization") String token, @Query("userId") String userid, @Query("cardId")String cardId,
                 @Query("lead")String lead,Callback<AddTag_Modele>addNote_modeleCallback);

    @GET("/card/additionalInfo/addLocationAndTime")
    void addlocationtime(@Header("Authorization") String token, @Query("userId") String userid, @Query("cardId")String cardId,
                 @Query("location")String location,@Query("time")String time,
                 Callback<AddLocationTime_Modele>addNote_modeleCallback);

    @GET("/card/additionalInfo/getAdditionalInfo")
    void getadditonalinfo(@Header("Authorization") String token, @Query("userId") String userid, @Query("cardId")String cardId,
                          Callback<AditonalInfo_Modele>addNote_modeleCallback);


    @PUT("/card/additionalInfo/updateAdditionalInfo")
    void update_addtionalinfo(@Header("Authorization") String token, @Query("userId") String userid, @Query("cardId")String cardId,
                              @Body TypedInput body, Callback<UpdateAddtionalInfo_Model>updateAddtionalInfo_modelCallback);

    @GET("/card/getReceivedCardDataByCardIdAndUserId")
    void getReceivedCardDetails(@Header("Authorization") String token, @Query("userId") String userid, @Query("cardId")String cardId,Callback<RecivedCard_model>recivedCard_modelCallback);

    @POST("/card/additionalInfo/addLink")
     void updateAdditional_link(@Header("Authorization") String token, @Query("userId") String userid, @Query("cardId")String cardId,
                                @Body TypedInput body,Callback<AdditionalUpdate_Model>additionalUpdate_modelCallback);

    @POST("/card/addCardManually")
    void addmanualcard(@Header("Authorization") String token,@Body TypedInput body,Callback<AddManualCard_model>addManualCard_modelCallback);

    @DELETE("/card/deleteReceivedCardByCardIdAndUserId")
    void deletereceivedcard(@Header("Authorization") String token, @Query("userId") String userid, @Query("cardId")String cardId,Callback<DeleteRecivedCard_Modle>deleteRecivedCard_modleCallback);

    @PUT("/card/changeMyCardColor")
    void changecardBackgroundcolor(@Header("Authorization") String token, @Query("cardId")String cardId, @Query("color")String color,
                                   @Body TypedInput body,Callback<CardBackgroundColorChnage_Model>cardBackgroundColorChnage_modelCallback);

    @POST("/user/search")
    void serachuserapi(@Header("Authorization") String token, @Body TypedInput body,Callback<SearchUser_Model>searchUser_modelCallback);


    @DELETE("/user/{userId}")
    void deleteaccount(@Header("Authorization") String token,@Path("userId")String userid,Callback<Deleteaccount_Model>deleteaccount_modelCallback);

    @Headers("Content-Type: text/ plain")
    @POST("/getBusinessCardInfo")
    void getBusinessCardData(@Header("Authorization") String token, @Body String body, Callback<BusinessCardInfo> cardModelCallback);

    @GET("/public/user/qualification")
    void getQalificationList(@Header("Authorization") String token,Callback<GetQualificaation_Model>getQualificaation_modelCallback);

    @POST("/event/addEventByUserId")
    void addevent(@Header("Authorization") String token,@Query("userId")String userId,@Body TypedInput body,Callback<AddEvent_Model>addEvent_modelCallback);

    @GET("/event/getAllEventsByUserId")
    void getAllevents(@Header("Authorization") String token,@Query("userId")String userId
            ,@Query("month")int month,Callback<GetAllEvent_Model>getAllEvent_modelCallback);

    @GET("/company")
    void getCompany(@Header("Authorization") String token,Callback<GetCompany_Model>getCompany_modelCallback);

    @PUT("/card/sendAutoShareValueByCardId")
    void autosharecard(@Header("Authorization") String token,@Query("cardId")String cardId,@Query("autoShare")String autoShare,@Body TypedInput body,
                       Callback<AutoShare_Model>autoShare_modelCallback);

    @PUT("/update-location/{userid}/{latitude}/{longitude}/")
    void updatelatlng(@Header("Authorization") String token,@Path("userid") String userid,@Path("latitude")String latitude,@Path("longitude")String longitude,
                      @Body TypedInput body
                     ,Callback<UpdateLatlng_Model>updateLatlng_modelCallback
    );

    @GET("/user/nearby-users/{userid}/{latitude}/{longitude}/")
    void nearby(@Header("Authorization") String token, @Path("userid") String userid, @Path("latitude")String latitude, @Path("longitude")String longitude,
                Callback<NearByUser_Model>nearByUser_modelCallback);

    @PUT("/user/password/{userid}")
    void changepassword_settings(@Header("Authorization") String token,@Path("userid") String userid,@Body TypedInput body,
                                 Callback<ChangePassword_Model>changePassword_modelCallback);

    @GET("/event/syncEventByEventId")
    void syncevent(@Header("Authorization") String token,@Query("eventId")String eventId,Callback<EventSynup_Model>eventSynup_modelCallback);

    @GET("/event/getAllNotificationByUserId")
    void getlistnotification(@Header("Authorization") String token,@Query("userId")String userId,Callback<ListNotification_Model>listNotification_modelCallback);

    @GET("/event/readNotificationByNotificationId")
    void readnotification(@Header("Authorization") String token,@Query("notificationId")String notificationId,Callback<ReadNotification_Model>listNotification_modelCallback);

    @GET("/event/sendNotificationToRequestViewProfileOfUser")
    void sendnotificationRequest(@Header("Authorization") String token,@Query("senderId")String senderId,@Query("receiverId")String receiverId,
                                 Callback<SendNotificationRequest_Model>sendNotificationRequest_modelCallback);


    @PUT("/event/viewProfileRequestAcceptOrNotByReceivedUser")
    void viewProfileRequestAcceptOrNotByReceivedUser(@Header("Authorization") String token,@Query("requestViewProfileId")String senderId,@Query("isAccept")String receiverId,
                                                     @Body TypedInput body, Callback<SendNotificationRequest_Model>sendNotificationRequest_modelCallback);

    @GET("/event/checkAlreadyRequestedOrNotForSearchUserProfile")
    void checkAlreadyRequestedOrNotForSearchUserProfile(@Header("Authorization") String token,@Query("senderId")String senderId,@Query("receiverId")String receiverId
    ,Callback<CheckRequested_Model>checkRequested_modelCallback
    );

    @GET("/user/askEmailToViewCardOnBrowser")
    void askEmailToViewCardOnBrowser(@Header("Authorization") String token, @Query("userId")String userId, @Query("setValue")int setValue, Callback<AskEmail_Model>askEmail_modelCallback);

    @GET("/user/setNotificationByUserId")
    void asknotification(@Header("Authorization") String token, @Query("userId")String userId, @Query("isNotification")int isNotification, Callback<AskNotification_Model>askEmail_modelCallback);


    @GET("/user/blockUserFromSearching")
    void blocksearchApo(@Header("Authorization") String token, @Query("userId") String userid, @Query("block")Boolean block,
                        Callback<BlockSearchModal>blockSearchCallback);


    @GET("/user/shareCardTemplate")
    void shareCardTemplate(@Header("Authorization") String token,@Query("userId") String userid,
                           @Query("template") String template,Callback<ShareCardTemplate_Model>shareCardTemplate_modelCallback);

    @POST("/payments")
    void postPaymentIdOnServer(@Header("Authorization") String token,@Body TypedInput body, Callback<GetPaymentResponce> getPaymentResponceCallback);


    @PUT("/user/mobile/{userid}/{mobile}")
    void updatemobilenumber(@Header("Authorization") String token,@Path("userid") String userid,
                            @Path("mobile") String mobile,@Body TypedInput body,Callback<UpdateMobile_Modele>updateMobile_modeleCallback);



    @PUT("/user/twoway-authentication/{userid}/{status}")
    void twowayauth(@Header("Authorization") String token,@Path("userid") String userid,
                            @Path("status") String mobile,@Body TypedInput body,Callback<TwoWayAuth_Model>twoWayAuth_modelCallback);


    @GET("/user/sharemyuserid")
    void sharemyuserid(@Header("Authorization") String token,@Query("userId") String userid,
                       @Query("userType") String userType,
                       Callback<ShareMyUserId_Model>shareMyUserId_modelCallback);


    @PUT("/user/change-password-request/{userid}/{mobile}")
    void requestotp_premium(@Header("Authorization") String token,@Path("userid") String userid,@Path("mobile") String mobile,@Body TypedInput body,
                            Callback<RequestOtp_Model>requestOtp_modelCallback);



    @PUT("/user/changePasswordForPremiumUser/{userid}/{otp}")
    void submitotp(@Header("Authorization") String token,@Path("userid") String userid,@Path("otp") String otp,@Body TypedInput body,
                            Callback<Submitotp_model>submitotp_modelCallback);


     @GET("/event/getUnreadCountOfNotification")
    void getunreadcount(@Header("Authorization") String token,@Query("userId") String userId,
                        Callback<UnreadNotficationCount_Model>unreadNotficationCount_modelCallback);

     @GET("/event/sendNotificationToShareMyUserId")
    void sendNotificationToShareMyUserId(@Header("Authorization") String token,@Query("senderId") String senderId,
                                         @Query("receiverId") String receiverId,Callback<ShareNotificationUserid_Model>shareNotificationUserid_modelCallback);
     @GET("/plans")
    void getplans(@Header("Authorization") String token,Callback<GEtPlanDetails>gEtPlanDetailsCallback);


     @POST("/orders")
    void generateorderid(@Header("Authorization") String token,@Body TypedInput body,Callback<GenrateOrderID_Model>genrateOrderID_modelCallback);


    @GET("/event/checkReceivedCardRemovedOrNotWhenClickOnNotification")
    void checknotificationreceivecard(@Header("Authorization") String token,@Query("senderId") String senderId,
                                      @Query("receiverId") String receiverId,@Query("cardId") String cardId
            ,Callback<CheckCardAvaiblability_Model>checkCardAvaiblability_modelCallback);


    @GET("/public/user/registerUser")
    void registration_otp(@Query("mobile") String mobile,@Query("isBusinessApp")boolean status,Callback<RegistrationOtp_Model>registrationOtp_modelCallback);


    @GET("/user/updateEmail/{userid}/{email}")
    void updateEmail(@Header("Authorization") String token,@Path("userid")String userid,@Path("email")String email,Callback<UpdateEmail_Model>updateEmail_modelCallback);

    @GET("/companyAdmin/getAllTemplateColors")
    void getAllTemplateColors(@Header("Authorization") String token,Callback<TemplateColorModel>templateColorModelCallback);

    @GET("/card/shareCardByUserId")
    void sharemail_sms(@Header("Authorization") String token, @Query("senderId") String senderId, @Query("receiverId") String receiverId,
                       @Query("cardId")String cardId, @Query("emailOrMobile")String emailOrMobile, Callback<ShareCard_Model>shareCard_modelCallback);

    @POST("/user/scanAndSaveQrCodeDataFromWeb")
    void desktoplogin(@Header("Authorization") String token,@Body TypedInput body,Callback<DesktopLogin_Model>desktopLogin_modelCallback);

    @DELETE("/event/mobile/{userId}/{eventId}")
    void delete_event(@Header("Authorization") String token,@Path("userId")String userid,@Path("eventId")String eventId,
                      Callback<DeleteEvent_Model>deleteEvent_modelCallback);

    @GET("/card/addCardInContactList")
    void addcontact(@Header("Authorization") String token,@Query("cardId") String cardId,@Query("isCardAddInContact") boolean isCardAddInContact,
                    Callback<ContactAdd_Model>contactAdd_modelCallback);

}

