package com.avoconnectcorporate.activity;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.LinearLayout;

import com.avoconnectcorporate.BottomSheetFragments.AddtionalLinkFragment;
import com.avoconnectcorporate.Fragments.ImportantLinkfragment;

import com.avoconnectcorporate.Fragments.MediaLinkFragment;
import com.avoconnectcorporate.Fragments.SoacialLinkTitleFragment;
import com.avoconnectcorporate.R;

public class SocialLinkAddActtivity extends AppCompatActivity {

    private BottomSheetBehavior mBottomSheetBehavior;
    String isimp="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_social_link_add_acttivity);
        LinearLayout bottomSheet =(LinearLayout) findViewById(R.id.bottom_sheet);
        mBottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);
        mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        mBottomSheetBehavior.setPeekHeight(0);

        Bundle bundle=getIntent().getExtras();
        if(bundle!=null)
        {
            if(bundle.getString("type")!=null)
            {
                String type=bundle.getString("type");
                if(type.equalsIgnoreCase("important_link"))
                {
                    ImportantLinkfragment importantLinkfragment = new ImportantLinkfragment();
                    importantLinkfragment.setCancelable(false);
                    importantLinkfragment.show(getSupportFragmentManager(), importantLinkfragment.getTag());
                }
                else if(type.equalsIgnoreCase("additional_link"))
                {
                    AddtionalLinkFragment importantLinkfragment = new AddtionalLinkFragment();
                    importantLinkfragment.setCancelable(false);
                    importantLinkfragment.show(getSupportFragmentManager(), importantLinkfragment.getTag());
                }
                else if(type.equalsIgnoreCase("media"))
                {
                    MediaLinkFragment importantLinkfragment = new MediaLinkFragment();
                    importantLinkfragment.setCancelable(false);
                    importantLinkfragment.show(getSupportFragmentManager(), importantLinkfragment.getTag());
                }
            }

        }
        else{
            SoacialLinkTitleFragment linkTitleFragment = new SoacialLinkTitleFragment();
            linkTitleFragment.setCancelable(false);
            linkTitleFragment.show(getSupportFragmentManager(), linkTitleFragment.getTag());
        }


    }
}
