package com.avoconnectcorporate.activity;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.avoconnectcorporate.R;
import com.bumptech.glide.Glide;
import com.github.chrisbanes.photoview.PhotoView;

import java.io.File;

public class ImagePopActivity extends AppCompatActivity {

    PhotoView image_view;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_pop);

        image_view=(PhotoView)findViewById(R.id.image_view);

        Bundle bundle=getIntent().getExtras();
        if(bundle!=null)
        {
            if(bundle.getString("url")!=null)
            {
                Glide.with(ImagePopActivity.this).load(bundle.getString("url")).into(image_view);
            }else if(bundle.getString("local_url")!=null)
            {
                File file=new File(bundle.getString("local_url"));
                Glide.with(ImagePopActivity.this).load(file).into(image_view);
            }
        }

    }
}
