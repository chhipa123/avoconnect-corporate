package com.avoconnectcorporate.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.avoconnectcorporate.Constants.Constant;
import com.avoconnectcorporate.Prefrence.UserPrefrence;
import com.avoconnectcorporate.R;
import com.avoconnectcorporate.Util.ProgressBarCustom;
import com.avoconnectcorporate.Util.Utils;
import com.avoconnectcorporate.api.RestClient;
import com.avoconnectcorporate.responsemodel.UpdateEmail_Model;

import retrofit.RetrofitError;
import retrofit.client.Response;

public class GeneratePopAskEmail extends AppCompatActivity implements View.OnClickListener {

    EditText et_email;
    TextView txt_reg;
    Utils utils;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_generate_pop_ask_email);
        utils=new Utils(GeneratePopAskEmail.this);
        et_email=(EditText)findViewById(R.id.et_email);
        txt_reg=(TextView)findViewById(R.id.txt_reg);

        txt_reg.setOnClickListener(this);
    }

    @Override
    public void onBackPressed() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.txt_reg:
                validation();
                break;
        }
    }

    private void validation() {
        String getemail=et_email.getText().toString();
        if(et_email.getText().toString().length()==0)
        {
            ProgressBarCustom.customDialog(this,getResources().getString(R.string.enter_email),false);
        }else if(new Utils(GeneratePopAskEmail.this).isvalidaEmail(getemail)==false)
        {
            ProgressBarCustom.customDialog(this,getResources().getString(R.string.enter_valid_email),false);
        }else{
            callApi();
        }
    }



    private void callApi() {


        final ProgressBarCustom progressBarCustom=new ProgressBarCustom(GeneratePopAskEmail.this);
        progressBarCustom.showProgress();

        try {
            String token = UserPrefrence.getInstance(GeneratePopAskEmail.this).getAuthtoken();
            token="bearer " + token;

            RestClient.getInstance(this).get().updateEmail(token,
                    UserPrefrence.getInstance(GeneratePopAskEmail.this).getUserid(),
                    et_email.getText().toString(),new retrofit.Callback<UpdateEmail_Model>() {

                @Override
                public void success(final UpdateEmail_Model res, Response response) {
                    progressBarCustom.cancelProgress();
                    if(res.getResponseMessage().equalsIgnoreCase(Constant.SUCCESS))
                    {

                            String userid=UserPrefrence.getInstance(GeneratePopAskEmail.this).getUserid();
                            UserPrefrence.getInstance(GeneratePopAskEmail.this).setIsLogin(userid);
                            Toast.makeText(GeneratePopAskEmail.this, res.getMessage(), Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(GeneratePopAskEmail.this, HomeActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            finish();
                    }else{
                        Toast.makeText(GeneratePopAskEmail.this, res.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void failure(final RetrofitError error) {
                    progressBarCustom.cancelProgress();
                    if (error != null) {

                        utils.showError(error);
                    }
                }
            });

        }catch (Exception e)
        {
            progressBarCustom.cancelProgress();
            //progress.cancelProgress();
            e.printStackTrace();
        }
    }
}
