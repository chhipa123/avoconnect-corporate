package com.avoconnectcorporate.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import com.avoconnectcorporate.Constants.Constant;
import com.avoconnectcorporate.Prefrence.UserPrefrence;
import com.avoconnectcorporate.R;
import com.avoconnectcorporate.Util.ProgressBarCustom;
import com.avoconnectcorporate.Util.Utils;
import com.avoconnectcorporate.api.RestClient;
import com.avoconnectcorporate.responsemodel.DesktopLogin_Model;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import org.json.JSONObject;

import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;
import retrofit.mime.TypedInput;

public class LoginWithQrCode extends AppCompatActivity {

    boolean isfirst=false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_with_qr_code);

        askPermission();
    }

    private void askPermission()
    {
        Dexter.withActivity(this)
                .withPermission(Manifest.permission.CAMERA)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {
                        // permission is granted, open the camera
                        IntentIntegrator integrator = new IntentIntegrator(LoginWithQrCode.this);
                        integrator.setOrientationLocked(true);
                        integrator.initiateScan();
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                        // check for permanent denial of permission
                        if (response.isPermanentlyDenied()) {
                            // navigate user to app settings
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if(result != null) {
            if(result.getContents() == null) {
                // Toast.makeText(this, "Cancelled", Toast.LENGTH_LONG).show();
                finish();
            } else {
                callApiLoginDesktop(result.getContents());
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }



    private void callApiLoginDesktop(String contents) {
        final ProgressBarCustom progressBarCustom=new ProgressBarCustom(LoginWithQrCode.this);
        progressBarCustom.showProgress();
        try {

            String token = UserPrefrence.getInstance(LoginWithQrCode.this).getAuthtoken();
            token="bearer " + token;
            JSONObject jsonObject3 = new JSONObject();
            jsonObject3.put("userId",UserPrefrence.getInstance(LoginWithQrCode.this).getUserid());
            jsonObject3.put("roleId", 5);
            jsonObject3.put("mobile", UserPrefrence.getInstance(LoginWithQrCode.this).getMobile());
            jsonObject3.put("encodedOtp", UserPrefrence.getInstance(LoginWithQrCode.this).getOtpnumnber().trim());
            jsonObject3.put("qrCode", contents);
            jsonObject3.put("accessToken", UserPrefrence.getInstance(LoginWithQrCode.this).getAuthtoken());
            jsonObject3.put("expiresIn", UserPrefrence.getInstance(LoginWithQrCode.this).getExpiresin());
            jsonObject3.put("jti", UserPrefrence.getInstance(LoginWithQrCode.this).getJti());
            jsonObject3.put("permission", UserPrefrence.getInstance(LoginWithQrCode.this).getPermission());
            jsonObject3.put("refreshToken", UserPrefrence.getInstance(LoginWithQrCode.this).getRefreshtoken());

            TypedInput in3 = new TypedByteArray("application/json", jsonObject3.toString().getBytes("UTF-8"));
            RestClient.getInstance(this).get().desktoplogin( token,in3,new retrofit.Callback<DesktopLogin_Model>() {

                @Override
                public void success(final DesktopLogin_Model res, Response response) {
                    progressBarCustom.cancelProgress();


                    if(res.getResponseMessage().equalsIgnoreCase(Constant.SUCCESS))
                    {

                        Handler handler = new Handler();
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(LoginWithQrCode.this, res.getMessage(), Toast.LENGTH_SHORT).show();
                                finish();
                            }
                        });
                    }else{
                        Toast.makeText(LoginWithQrCode.this, res.getMessage(), Toast.LENGTH_SHORT).show();
                        finish();
                    }

                }
                @Override
                public void failure(final RetrofitError error) {
                    progressBarCustom.cancelProgress();
                    error.printStackTrace();
                    if (error != null) {
                        Utils utils=new Utils(LoginWithQrCode.this);
                        utils.showError(error);
                    }

                }
            });


        }catch (Exception e)
        {
            progressBarCustom.cancelProgress();
            //progress.cancelProgress();
            e.printStackTrace();
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

}
